$(window).load(function(){
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
		$('body').addClass('ios');
	} else{
		$('body').addClass('web');
	};
	$('body').removeClass('loaded'); 
});
/* viewport width */
function viewport(){
	var e = window, 
		a = 'inner';
	if ( !( 'innerWidth' in window ) )
	{
		a = 'client';
		e = document.documentElement || document.body;
	}
	return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
};
/* viewport width */
$(function(){
	/* placeholder*/	   
	$('input, textarea').each(function(){
 		var placeholder = $(this).attr('placeholder');
 		$(this).focus(function(){ $(this).attr('placeholder', '');});
 		$(this).focusout(function(){			 
 			$(this).attr('placeholder', placeholder);  			
 		});
 	});
	/* placeholder*/

	$('.button-nav').click(function(){
		$(this).toggleClass('active'), 
		$('.main-nav-list').slideToggle(); 
		return false;
	});
	
	/* components */
	
	/*
	
	if($('.styled').length) {
		$('.styled').styler();
	};
	if($('.fancybox').length) {
		$('.fancybox').fancybox({
			margon  : 10,
			padding  : 10
		});
	};
	if($('.slick-slider').length) {
		$('.slick-slider').slick({
			dots: true,
			infinite: false,
			speed: 300,
			slidesToShow: 4,
			slidesToScroll: 4,
			responsive: [
				{
				  breakpoint: 1024,
				  settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true,
					dots: true
				  }
				},
				{
				  breakpoint: 600,
				  settings: "unslick"
				}				
			]
		});
	};
	if($('.scroll').length) {
		$(".scroll").mCustomScrollbar({
			axis:"x",
			theme:"dark-thin",
			autoExpandScrollbar:true,
			advanced:{autoExpandHorizontalScroll:true}
		});
	};
	
	*/
	
	/* components */
	
	

});

var handler = function(){
	
	var height_footer = $('footer').height();	
	var height_header = $('header').height();		
	//$('.content').css({'padding-bottom':height_footer+40, 'padding-top':height_header+40});
	
	
	var viewport_wid = viewport().width;
	var viewport_height = viewport().height;
	
	if (viewport_wid <= 991) {
		
	}
	
}
$(window).bind('load', handler);
$(window).bind('resize', handler);

$(document).ready(function(){
    // disable carousel cycling
    $('.carousel').carousel({ interval: false });
});


var map;
      function initMap() {
		  var myLatLng = {lat: 51.166996, lng: 71.440790};
        map = new google.maps.Map(document.getElementById('map'), {
          center: myLatLng,
          zoom: 16
        });
		  var marker = new google.maps.Marker({
    		position: myLatLng,
			map: map,
    		icon:'../img/marker.png',
			 title: 'Астана, ул. Кенесары, 52'
  		});
		  var myLatLng2 = {lat: 43.2431871, lng: 76.9488217};
        map = new google.maps.Map(document.getElementById('map-2'), {
          center: myLatLng2,
          zoom: 16
        });
		  var marker = new google.maps.Marker({
    		position: myLatLng2,
			map: map,
    		icon:'../img/marker.png',
			  title: 'Алматы, ул. Фурманова, 174А'
  		});
		
      }

	
(function($) {
$(function() {

  $('.catalog-select').styler();

});
})(jQuery);
$(document).ready(function(){
        $(".jq-selectbox__select-text").html(function(index, text) {
        return text.replace(new RegExp("популярности", 'g'), "<span style='font-weight:bold;'>популярности</span>")
        });
    });
$(document).ready(function(){
        $(".selected.sel").html(function(index, text) {
        return text.replace(new RegExp("популярности", 'g'), "<span style='font-weight:bold;'>популярности</span>")
        });
    });
$(document).ready(function() {
    $("a.fancybox").fancybox();
});



// Without JQuery

jQuery("#slider").slider({
	min: 0,
	max: 20000,
	values: [0,20000],
	range: true,
	stop: function(event, ui) {
		jQuery("input#minCost").val(jQuery("#slider").slider("values",0));
		jQuery("input#maxCost").val(jQuery("#slider").slider("values",1));
    },
    slide: function(event, ui){
		jQuery("input#minCost").val(jQuery("#slider").slider("values",0));
		jQuery("input#maxCost").val(jQuery("#slider").slider("values",1));
    }
});

jQuery("input#minCost").change(function(){
	var value1=jQuery("input#minCost").val();
	var value2=jQuery("input#maxCost").val();

    if(parseInt(value1) > parseInt(value2)){
		value1 = value2;
		jQuery("input#minCost").val(value1);
	}
	jQuery("#slider").slider("values",0,value1);	
});

	
jQuery("input#maxCost").change(function(){
	var value1=jQuery("input#minCost").val();
	var value2=jQuery("input#maxCost").val();
	
	if (value2 > 20000) { value2 = 20000; jQuery("input#maxCost").val(20000)}

	if(parseInt(value1) > parseInt(value2)){
		value2 = value1;
		jQuery("input#maxCost").val(value2);
	}
	jQuery("#slider").slider("values",1,value2);
});

$(document).ready(function() {
	$(".fancybox").fancybox({
		openEffect	: 'none',
		closeEffect	: 'none'
	});
});
jQuery(function($) {

$.mask.definitions['~']='[+-]';

$('#date').mask('99/99/9999');

$('#phone').mask('+7( 9 9 9) 9 9 9 9 9 9 9');

$('#phoneext').mask("(999) 999-9999? x99999");

$("#tin").mask("99-9999999");

$("#ssn").mask("999-99-9999");

$("#product").mask("a*-999-a999");

$("#eyescript").mask("~9.99 ~9.99 999");

})

$('.selectpicker').selectpicker({
  style: 'btn-info',
  size: 4
});
$('.catalog-content__navigation__text__arrow').click(function(){
	
	
	
		$('.item-1-wrapp-2,.item-1-wrapp-1').slideToggle();
		
	
	
})

$('.reviev-btn').click(function(){

		$('.reviev-btn').text('Свернуть');
		$('.reviev__content__wrapper:nth-child(n+6)').slideToggle();
		
		
	
})

$('.arr-2').click(function(){
	
		
	
		$('.slide-form-content').slideToggle();
		
	
	
})
$(".carousel").on("touchstart", function(event){
        var xClick = event.originalEvent.touches[0].pageX;
    $(this).one("touchmove", function(event){
        var xMove = event.originalEvent.touches[0].pageX;
        if( Math.floor(xClick - xMove) > 5 ){
            $(this).carousel('next');
        }
        else if( Math.floor(xClick - xMove) < -5 ){
            $(this).carousel('prev');
        }
    });
    $(".carousel").on("touchend", function(){
            $(this).off("touchmove");
    });
});

$(' .dropdown-menu>li:first-child>a').click(function(){
		$('.item-map-1').removeClass('map-size')
		$('.item-map-2').addClass('map-size')
	})
$(' .dropdown-menu>li:nth-of-type(2)>a').click(function(){
		$('.item-map-2').removeClass('map-size')
		$('.item-map-1').addClass('map-size')
	})
